#ifndef __EDGE_DATA_HPP__
#define __EDGE_DATA_HPP__

#include <string>

template <typename ...Tp>
class EdgeData : public Tp ... {
  std::string str;
public:
  void set_string(const std::string &s) { str = s; }
  std::string get_string() const { return str; }
};

#endif // __EDGE_DATA_HPP__
