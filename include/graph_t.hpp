#ifndef __GRAPH_T_HPP__
#define __GRAPH_T_HPP__

#include <exception>
#include <memory>
#include <map>
#include <vector>
#include <string>
#include <queue>
#include <unordered_map>
#include <algorithm>

// Searches an element in the container, if it exists, it is removed
template<class C, class E>
void find_remove(C &cont, E elem)
{
    auto pos = cont.find(elem);
    if (pos != end(cont))
        cont.erase(pos);
}

// Exception : wrong node identifier
class NodeNotFound {
    const int id;
public:
    NodeNotFound(int node_id) : id(node_id) {}
    std::string msg() const {
        std::string m = "Node " + std::to_string(id) + " not found";
        return m;
    }
};

// Exception : wrong node identifier 
class EdgeNotFound {
    const int id;
public:
    EdgeNotFound(int edge_id) : id(edge_id) {}
    std::string msg() const {
        std::string m = "Edge " + std::to_string(id) + " not found";
        return m;
    }
};

template<class ND, class ED>
class Graph {
    struct Node {
        int node_id;
        std::shared_ptr<ND> data;
    };
    struct Edge {
        int edge_id;
        int source_id;
        int dest_id;
        std::shared_ptr<ED> data;
    };

    /* data structures */
    std::map<int, Node> nodes;
    std::map<int, Edge> edges;
    std::map<int, std::vector<int>> dests;
    int id_counter;
    int edge_counter;

public:

    Graph() {
      id_counter = 0;
      edge_counter = 0;
    }

    Graph(const Graph &other) {
        id_counter = other.id_counter;
        edge_counter = other.edge_counter;
        for (auto node : other.nodes) {
            nodes[node.first] = node.second;
        }
        for (auto edge : other.edges) {
            edges[edge.first] = edge.second;
        }
        for (auto dest : other.dests) {
            dests[dest.first] = std::vector<int>();
            for (auto d : dest.second) {
                dests[dest.first].push_back(d);
            }
        }
    }

    Graph deep_copy() const {
        Graph copy;
        for (const auto& [id, node] : nodes) {
            copy.nodes[id] = Node{id, std::make_shared<ND>(*node.data)};
        }

        for (const auto& [id, edge] : edges) {
            copy.edges[id] = Edge{id, edge.source_id, edge.dest_id, std::make_shared<ED>(*edge.data)};
        }

        for (const auto& [source, dests] : dests) {
            copy.dests[source] = std::vector<int>();
            for (auto d : dests) {
                copy.dests[source].push_back(d);
            }
        }

        return copy;
    }

    inline int add_node(const ND &m) {
      Node node;
      node.node_id = id_counter;
      node.data = std::make_shared<ND>(m);
      nodes[id_counter] = node;
      dests[id_counter] = std::vector<int>();
      return id_counter++;
    }

    inline bool node_exist(int id) const {
        if (nodes.find(id) == nodes.end()) {
            return false;
        }
        return true;
    }

    inline int add_edge(const ED &m, int source_id, int dest_id) {
        if (!node_exist(source_id)) {
            throw NodeNotFound(source_id);
        }
        if (!node_exist(dest_id)) {
            throw NodeNotFound(dest_id);
        }

        Edge edge;
        edge.edge_id = edge_counter;
        edge.data = std::make_shared<ED>(m);
        *edge.data = m;
        edge.source_id = source_id;
        edge.dest_id = dest_id;
        edges[edge_counter] = edge;
        dests[source_id].push_back(dest_id);
        return edge_counter++;
    }

    inline void remove_node(int node_id) {
        if (!node_exist(node_id)) {
            throw NodeNotFound(node_id);
        }
        for (auto edge_id : dests[node_id]) {
            edges.erase(edge_id);
        }
        dests.erase(node_id);
        nodes.erase(node_id);
    }

    inline int search_node(const ND &m) const {
        for (auto node : nodes) {
            if (node.second.data == m) {
                return node.first;
            }
        }
        return -1;
    }
    
    inline std::shared_ptr<ND> get_node_data(int node_id) const {
        if (!node_exist(node_id)) {
            throw NodeNotFound(node_id);
        }
        return nodes.at(node_id).data;
    }

    inline std::shared_ptr<ED> get_edge_data(int edge_id) const {
        if (edges.find(edge_id) == edges.end()) {
            throw EdgeNotFound(edge_id);
        }
        return edges.at(edge_id).data;
    }

    inline int get_edge_source(int edge_id) const {
        if (edges.find(edge_id) == edges.end()) {
            throw EdgeNotFound(edge_id);
        }
        return edges.at(edge_id).source_id;
    }
    
    inline int get_edge_dest(int edge_id) const {
        if (edges.find(edge_id) == edges.end()) {
            throw EdgeNotFound(edge_id);
        }
        return edges.at(edge_id).dest_id;
    }

    std::vector<int> get_successors(int node_id) const {
        if (!node_exist(node_id)) {
            throw NodeNotFound(node_id);
        }
        return dests.at(node_id);
    }
    
    std::vector<int> get_predecessors(int node_id) const {
        if (!node_exist(node_id)) {
            throw NodeNotFound(node_id);
        }
        std::vector<int> preds;
        // Visit all edges to find predecessors
        for (auto edge : edges) {
            if (edge.second.dest_id == node_id) {
                preds.push_back(edge.second.source_id);
            }
        }
        return preds;
    }

    using Path=std::vector<int>;

    std::vector<Path> all_paths(int from, int to) const
        {
            std::vector<Path> paths;
            std::vector<int> visited;
            Path path;
            all_paths(from, to, visited, path, paths);
            return paths;
        }

    void all_paths(int from, int to, std::vector<int> &visited, Path &path, std::vector<Path> &paths) const
        {
            visited.push_back(from);
            path.push_back(from);
            if (from == to) {
                paths.push_back(path);
            } else {
                for (auto dest : get_successors(from)) {
                    if (std::find(visited.begin(), visited.end(), dest) == visited.end()) {
                        all_paths(dest, to, visited, path, paths);
                    }
                }
            }
            visited.pop_back();
            path.pop_back();
        }

    //// Utilse une fonction pour evaluer la distance d'une edge
    //template<typename F>
    //Path shortest_path(int from, int to) const
    //    {
    //        std::vector<Path> paths = all_paths(from, to);
    //        if (paths.size() == 0) {
    //            throw std::runtime_error("No path found");
    //        }
    //        Path shortest = paths[0];
    //        for (auto path : paths) {
    //            if (path.size() < shortest.size()) {
    //                shortest = path;
    //            }
    //        }
    //        return shortest;
    //    }

    template<typename F>
    Path shortest_path(int from, int to, F f) const {
        if (!node_exist(from) || !node_exist(to)) {
            throw std::runtime_error("One or both nodes not found in the graph.");
        }

        // Priority queue to store pairs of (cost, node_id)
        std::priority_queue<std::pair<int, int>, std::vector<std::pair<int, int>>, std::greater<std::pair<int, int>>> pq;

        // Distance map to store the minimum distance to each node
        std::unordered_map<int, int> dist;
        for (const auto& node : nodes) {
            dist[node.first] = std::numeric_limits<int>::max();
        }
        dist[from] = 0;

        // Predecessor map to reconstruct the path later
        std::unordered_map<int, int> prev;
        
        // Initialize the priority queue with the start node
        pq.push({0, from});

        while (!pq.empty()) {
            auto [current_distance, current_node] = pq.top();
            pq.pop();

            // If we reach the destination, stop the loop
            if (current_node == to) {
                break;
            }

            // Visit each successor of the current node
            for (int dest : get_successors(current_node)) {
                // Use the function `f` to calculate the weight of the edge between current_node and dest
                int edge_id = find_edge(current_node, dest);
                if (edge_id == -1) continue; // If no edge, skip

                int weight = f(*edges.at(edge_id).data);
                int distance = current_distance + weight;

                // If a shorter path to the successor has been found, update it
                if (distance < dist[dest]) {
                    dist[dest] = distance;
                    prev[dest] = current_node;
                    pq.push({distance, dest});
                }
            }
        }

        // Reconstruct the path by backtracking from the destination
        if (dist[to] == std::numeric_limits<int>::max()) {
            throw std::runtime_error("No path found");
        }

        std::vector<int> path;
        for (int at = to; at != from; at = prev[at]) {
            path.push_back(at);
        }
        path.push_back(from);
        std::reverse(path.begin(), path.end());

        return path;
    }

private:
    int find_edge(int source, int dest) const {
        for (const auto& pair : edges) {
            const auto& edge = pair.second;
            if (edge.source_id == source && edge.dest_id == dest) {
                return edge.edge_id;
            }
        }
        return -1; // Edge not found
    }
};


#endif
