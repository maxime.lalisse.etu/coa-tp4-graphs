#ifndef __ROUTE_LENGTH_HPP__
#define __ROUTE_LENGTH_HPP__

class RouteLength {
  double l;
public:
  void set_length(double len) { l = len; }
  double get_length(double len) const { return l; }
};

#endif // __ROUTE_LENGTH_HPP__
