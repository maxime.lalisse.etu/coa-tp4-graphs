#include <catch2/catch_test_macros.hpp>
#include "graph_t.hpp"
#include <iostream>

#include "edge_data.hpp"
#include "route_length.hpp"

using namespace std;

SCENARIO("Graph properties", "[graph t]")
{
    GIVEN("A graph with some items") {
        Graph<string, string> g;
        auto a = g.add_node("A");
        auto b = g.add_node("B");
        auto c = g.add_node("C");
        auto d = g.add_node("D");
        auto ab = g.add_edge("msg-ab", a,b);
        auto ac = g.add_edge("msg-ac", a,c);
        auto bd = g.add_edge("msg-bd", b,d);
        auto cd = g.add_edge("msg-cd", c,d);

        REQUIRE(*g.get_node_data(a) == "A");
        REQUIRE(*g.get_node_data(b) == "B");
        REQUIRE(*g.get_node_data(c) == "C");
        REQUIRE(*g.get_node_data(d) == "D");

        REQUIRE(ab == 0);
        REQUIRE(ac == 1);

        REQUIRE(*g.get_edge_data(ab) == "msg-ab");
        REQUIRE(*g.get_edge_data(ac) == "msg-ac");
        REQUIRE(*g.get_edge_data(cd) == "msg-cd");
        REQUIRE(*g.get_edge_data(bd) == "msg-bd");

        vector<int> succ_a = {b, c};
        REQUIRE(g.get_successors(a) == succ_a);
        vector<int> succ_b = {d};
        REQUIRE(g.get_successors(b) == succ_b);
        vector<int> prec_d = {b, c};
        REQUIRE(g.get_predecessors(d) == prec_d);

        WHEN("Copying the graph") {
            Graph g1(g);
            THEN ("We obtain the same graph, with the same indexes") {
                REQUIRE(g1.get_predecessors(d) == prec_d);
            }
        }
        WHEN("Changing the copy") {
            Graph g2(g);
            g2.add_edge("msg-bc", b,c);
            THEN ("The original is not changed") {
                REQUIRE(g.get_successors(b).size() == 1);
                REQUIRE(g2.get_successors(b).size() == 2);
            }
        }
    }
}

SCENARIO("Computing paths", "[graph t]")
{
    GIVEN("A graph with some elements and no loops") {
        Graph<string, string> g;
        int a = g.add_node("A");
        int b = g.add_node("B");
        int c = g.add_node("C");
        int d = g.add_node("D");
        int e = g.add_node("E");

        g.add_edge("ab", a, b);
        g.add_edge("ac", a, c);
        g.add_edge("bc", b, c);
        g.add_edge("bd", b, d);
        g.add_edge("cd", c, d);
        g.add_edge("de", d, e);
        g.add_edge("ce", c, e);

        WHEN ("Computing all paths") {
            auto paths = g.all_paths(a, e);
            REQUIRE(paths.size() == 5);
        }
    }
}

SCENARIO("Shortest paths", "[graph t]")
{
    GIVEN("A graph with some elements and no loops") {
        Graph<string, string> g;
        int a = g.add_node("A");
        int b = g.add_node("B");
        int c = g.add_node("C");
        int d = g.add_node("D");
        int e = g.add_node("E");

        g.add_edge("ab", a, b);
        g.add_edge("ac", a, c);
        g.add_edge("bc", b, c);
        g.add_edge("bd", b, d);
        g.add_edge("cd", c, d);
        g.add_edge("de", d, e);
        g.add_edge("ce", c, e);

        WHEN ("Computing all paths") {
            auto edgeLength = [](string edge_data) { return 1.0; };
            auto paths = g.all_paths(a, e);
            REQUIRE(paths.size() == 5);
            REQUIRE(g.shortest_path(a,b,edgeLength).size() == 2);
            REQUIRE(g.shortest_path(a,d,edgeLength).size() == 3);
            REQUIRE(g.shortest_path(a,e,edgeLength).size() == 3);
        }
    }
}

using EdgeProps = EdgeData<RouteLength>;

SCENARIO("Decorators", "[graph t]")
{
    GIVEN("A graph with some items") {

        Graph<string, EdgeProps> g;
        int a = g.add_node("A");
        int b = g.add_node("B");
        int c = g.add_node("C");
        int d = g.add_node("D");
        EdgeProps _ab, _ac, _bd, _cd;
        _ab.set_string("msg-ab");
        _ac.set_string("msg-ac");
        _bd.set_string("msg-bd");
        _cd.set_string("msg-cd");
        _ab.set_length(1.0);
        _ac.set_length(2.0);
        _bd.set_length(3.0);
        _cd.set_length(4.0);
        int ab = g.add_edge(_ab, a,b);
        int ac = g.add_edge(_ac, a,c);
        int bd = g.add_edge(_bd, b,d);
        int cd = g.add_edge(_cd, c,d);

        REQUIRE(*g.get_node_data(a) == "A");
        REQUIRE(*g.get_node_data(b) == "B");
        REQUIRE(*g.get_node_data(c) == "C");
        REQUIRE(*g.get_node_data(d) == "D");

        REQUIRE(ab == 0);
        REQUIRE(ac == 1);

        EdgeProps __ab = *g.get_edge_data(ab);
        EdgeProps __ac = *g.get_edge_data(ac);
        EdgeProps __cd = *g.get_edge_data(cd);
        EdgeProps __bd = *g.get_edge_data(bd);
        REQUIRE(__ab.get_string() == "msg-ab");
        REQUIRE(__ac.get_string() == "msg-ac");
        REQUIRE(__cd.get_string() == "msg-cd");
        REQUIRE(__bd.get_string() == "msg-bd");

        vector<int> succ_a = {b, c};
        REQUIRE(g.get_successors(a) == succ_a);
        vector<int> succ_b = {d};
        REQUIRE(g.get_successors(b) == succ_b);
        vector<int> prec_d = {b, c};
        REQUIRE(g.get_predecessors(d) == prec_d);

        WHEN("Copying the graph") {
            Graph g1(g);
            THEN ("We obtain the same graph, with the same indexes") {
                REQUIRE(g1.get_predecessors(d) == prec_d);
            }
        }
        WHEN("Changing the copy") {
            Graph g2(g);
            EdgeProps _bc;
            _bc.set_string("msg-bc");
            _bc.set_length(5.0);
            g2.add_edge(_bc, b,c);
            THEN ("The original is not changed") {
                REQUIRE(g.get_successors(b).size() == 1);
                REQUIRE(g2.get_successors(b).size() == 2);
            }
        }
    }
}

// Test deep copy
SCENARIO("Deep copy", "[graph t]")
{
    GIVEN("A graph with some items") {
        Graph<string, EdgeProps> g;
        int a = g.add_node("A");
        int b = g.add_node("B");
        int c = g.add_node("C");
        int d = g.add_node("D");
        EdgeProps _ab, _ac, _bd, _cd;
        _ab.set_string("msg-ab");
        _ac.set_string("msg-ac");
        _bd.set_string("msg-bd");
        _cd.set_string("msg-cd");
        _ab.set_length(1.0);
        _ac.set_length(2.0);
        _bd.set_length(3.0);
        _cd.set_length(4.0);
        int ab = g.add_edge(_ab, a,b);
        int ac = g.add_edge(_ac, a,c);
        int bd = g.add_edge(_bd, b,d);
        int cd = g.add_edge(_cd, c,d);

        REQUIRE(*g.get_node_data(a) == "A");
        REQUIRE(*g.get_node_data(b) == "B");
        REQUIRE(*g.get_node_data(c) == "C");
        REQUIRE(*g.get_node_data(d) == "D");

        REQUIRE(ab == 0);
        REQUIRE(ac == 1);

        EdgeProps __ab = *g.get_edge_data(ab);
        EdgeProps __ac = *g.get_edge_data(ac);
        EdgeProps __cd = *g.get_edge_data(cd);
        EdgeProps __bd = *g.get_edge_data(bd);
        REQUIRE(__ab.get_string() == "msg-ab");
        REQUIRE(__ac.get_string() == "msg-ac");
        REQUIRE(__cd.get_string() == "msg-cd");
        REQUIRE(__bd.get_string() == "msg-bd");

        vector<int> succ_a = {b, c};
        REQUIRE(g.get_successors(a) == succ_a);
        vector<int> succ_b = {d};
        REQUIRE(g.get_successors(b) == succ_b);
        vector<int> prec_d = {b, c};
        REQUIRE(g.get_predecessors(d) == prec_d);

        WHEN("Copying the graph") {
            Graph g1(g);
            THEN ("We obtain the same graph, with the same indexes") {
                REQUIRE(g1.get_predecessors(d) == prec_d);
            }
        }

        WHEN("Changing the copy") {
            Graph g2(g);
            EdgeProps _bc;
            _bc.set_string("msg-bc");
            _bc.set_length(5.0);
            g2.add_edge(_bc, b,c);
            THEN ("The original is not changed") {
                REQUIRE(g.get_successors(b).size() == 1);
                REQUIRE(g2.get_successors(b).size() == 2);
            }
        }
    }
}
